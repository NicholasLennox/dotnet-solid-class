﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.OCP.End
{
    internal class TagPost : Post
    {
        public override void CreatePost(Database database, string text)
        {
            database.AddTagPost(text); // Different behaviour depending on child
        }
    }
}
