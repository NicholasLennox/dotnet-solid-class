﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.OCP.End
{
    internal class Post
    {
        public virtual void CreatePost(Database database, string text)
        {
            database.AddPost(text); // Default/base behaviour
        }
    }
}
