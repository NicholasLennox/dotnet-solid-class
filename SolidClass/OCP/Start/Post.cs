﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.OCP.Start
{
    internal class Post
    {
        public void CreatePost(Database database, string text)
        {
            // Will need to come back and change this everytime we add a new post
            if (text.StartsWith("#"))
            {
                database.AddTagPost(text);
            } else
            {
                database.AddPost(text);
            }
        }
    }
}
