﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.SRP.End
{
    internal class Post
    {
        ErrorLogger logger = new ErrorLogger(); // Compose 

        public void CreatePost(Database database, string text)
        {
            try
            {
                database.AddPost(text);
            } catch (Exception ex)
            {
                logger.Log(ex.Message);
            }
        }
    }
}
