﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.SRP.End
{
    internal class ErrorLogger
    {
        public void Log(string text)
        {
            File.WriteAllText("Errors.txt", text);
        }
    }
}
