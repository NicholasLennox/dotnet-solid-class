﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.SRP.Start
{
    internal class Post
    {
        public void CreatePost(Database database, string text)
        {
            try
            {
                database.AddPost(text);
            } catch (Exception ex)
            {
                File.WriteAllText("Errors.txt", ex.Message); // Writing responsibilty
            }
        }
    }
}
