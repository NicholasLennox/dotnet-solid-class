﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.SRP.Start
{
    internal class Database
    {
        internal void AddPost(string text)
        {
            throw new Exception("Something went wrong with Add Post");
        }

        internal void AddTagPost(string text)
        {
            throw new Exception("Something went wrong with Add Tag Post");
        }

        internal List<string> GetUnhandledPosts()
        {
            throw new Exception("Many beeps, some boops");
        }

        internal void NotifyUser(string user)
        {
            throw new Exception("Beep boop");
        }
    }
}
