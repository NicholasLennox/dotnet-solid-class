﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.LSP.Start
{
    internal class MentionPost : Post
    {
        public void CreateMentionPost(Database database, string text)
        {
            string user = ParseUser(text);
            database.NotifyUser(user);
            database.AddPost(text);
        }

        private string ParseUser(string text)
        {
            return "Username";
        }
    }
}
