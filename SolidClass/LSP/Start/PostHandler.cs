﻿using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.LSP.Start
{
    internal class PostHandler
    {
        Database database = new Database();

        public void HandlePost()
        {
            List<string> unhandledPosts = database.GetUnhandledPosts();
            foreach (string unhandled in unhandledPosts)
            {
                Post post;
                // Encapsulate the different types of posts
                if(unhandled.StartsWith("#"))
                {
                    post = new TagPost();
                } else if (unhandled.StartsWith("@"))
                {
                    post = new MentionPost();
                } else
                {
                    post = new Post();
                }
                post.CreatePost(database, unhandled); // Call the behaviour
            }
        }
    }
}
