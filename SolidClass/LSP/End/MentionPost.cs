﻿using SolidClass.LSP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.LSP.End
{
    internal class MentionPost : Post
    {
        public override void CreatePost(SRP.Start.Database database, string text)
        {
            string user = ParseUser(text);
            database.NotifyUser(user);
            base.CreatePost(database, text);
        }

        private string ParseUser(string text)
        {
            return "Username";
        }
    }
}
