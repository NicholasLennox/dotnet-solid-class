﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace SolidClass.DIP.End
{
    internal class TextErrorLogger : ILogger
    {
        public void Log(string message)
        {
            File.WriteAllText("Errors.txt", message);
        }
    }
}
