﻿using SolidClass.SRP.End;
using SolidClass.SRP.Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidClass.DIP.End
{
    internal class Post
    {
        ILogger logger; // Invert dependency

        public Post(ILogger logger)
        {
            this.logger = logger; // Inject dependency
        }

        public void CreatePost(Database database, string text)
        {
            try
            {
                database.AddPost(text);
            } catch (Exception ex)
            {
                logger.Log(ex.Message);
            }
        }
    }
}
